import java.util.Map;

public class BrandCategory {

    private int brandCategoryId;
    private int brandId;
    private int categoryId;

    private Map<Integer,Brand> brandListByBrandCId;
    private Map<Integer,Category> categoryListByBrandCId;

    //get the list of all Brands based on category
    public Map<Integer, Brand> getBrandListByBrandCId() {
        return brandListByBrandCId;
    }

    public void setBrandListByBrandCId(Map<Integer, Brand> brandListByBrandCId) {
        this.brandListByBrandCId = brandListByBrandCId;
    }

    //get the list of all categories based on Brand
    public Map<Integer, Category> getCategoryListByBrandCId() {
        return categoryListByBrandCId;
    }

    public void setCategoryListByBrandCId(Map<Integer, Category> categoryListByBrandCId) {
        this.categoryListByBrandCId = categoryListByBrandCId;
    }

    public int getBrandCategoryId() {
        return brandCategoryId;
    }

    public void setBrandCategoryId(int brandCategoryId) {
        this.brandCategoryId = brandCategoryId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

}
