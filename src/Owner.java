import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Owner {

    public static void main(String args[]){

        displayAllBrandNames();
        addNewBrand();
        deleteBrand( 2);
    }

    //Display All the existing brands
    private static void displayAllBrandNames(){
        List<Brand> brandList = new ArrayList<Brand>();

        //displayExisting BrandNames
        brandList = BrandServices.getAllBrandsFromDAO();
        Iterator<Brand> iterator = brandList.iterator();
        System.out.println("Brand Id "+"    "+"Brand Name");
        System.out.println("------------------------------");
        while(iterator.hasNext()){
            Brand brand = iterator.next();
            System.out.println(brand.getBrandId()+"         "+brand.getBrandName());
        }

    }


    //Add New Brand
    private static void addNewBrand(){
        //add new Brand
        int existingCount = BrandServices.getAllBrandsFromDAO().size();
        Brand addNewBrand = new Brand(existingCount+1,"Newbrand"+(existingCount+1),"newdesc");
        BrandServices.addBrand(addNewBrand);

        List<Brand> brandList = addNewBrand.getBrandArrayList();

        //display the latest collection using Java Stream
        System.out.println("Brand Id "+"    "+"Brand Name");
        System.out.println("------------------------------");
        brandList.stream().forEach(s->System.out.println(s.getBrandId() +"      " +s.getBrandName()));
    }

    //Delete New Brand
    private static void deleteBrand(int brandId){
        //delete Brand
        BrandServices.deleteBrand(brandId);
        List<Brand> brandList = BrandServices.getAllBrandsFromDAO();

        //display the latest collection using Java Stream
        System.out.println("Brand Id "+"    "+"Brand Name");
        System.out.println("------------------------------");
        brandList.stream().forEach(s->System.out.println(s.getBrandId() +"      " +s.getBrandName()));

    }
}
