import java.util.*;

public class BrandServices{


    public static Brand MAX = new Brand(1,"Max","description");
    public static Brand BIBA = new Brand(2,"BIBA","dedscription");
    public static Brand LEE = new Brand(3,"LEE","description");
    public static Brand LIFESTYLE = new Brand(4,"Life Style","description");
    public static Brand LEVIS = new Brand(5,"Levis","description");
    public static Brand AURELIA = new Brand(6,"Aurelia","description");


    public static List<Brand> getAllBrandsFromDAO(){
        List<Brand> brandList = new ArrayList<Brand>();
        brandList.add(MAX);
        brandList.add(BIBA);
        brandList.add(LEE);
        brandList.add(LIFESTYLE);
        brandList.add(LEVIS);
        brandList.add(AURELIA);
        return brandList;
    }

    //For faster retrieval
   public static Map<Integer,Brand> getAllBrandsFromDAOUsingHashMap(){
        Map<Integer,Brand> brandList = new HashMap<Integer,Brand>();
        brandList.put(MAX.getBrandId(),MAX);
        brandList.put(BIBA.getBrandId(),BIBA);
        brandList.put(LEE.getBrandId(),LEE);
        brandList.put(LIFESTYLE.getBrandId(),LIFESTYLE);
        brandList.put(LEVIS.getBrandId(),LEVIS);
        brandList.put(AURELIA.getBrandId(),AURELIA);
        return brandList;
    }


    //addNew Brand
    public static void addBrand(Brand brand){
        List<Brand> list = getAllBrandsFromDAO();
        list.add(brand);
        brand.setBrandArrayList(list);
        System.out.println("Size from add  = "+ list.size());
    }

    //getBrand details by brandId
    public static Brand getBrandById(Integer brandId){
        Map<Integer,Brand> list = getAllBrandsFromDAOUsingHashMap();

        Optional<Map.Entry<Integer, Brand>> brand = list.entrySet().stream().filter( c-> c.getKey().equals(brandId)).findFirst();
        if(brand.isPresent()) {
            return brand.get().getValue();
        }
        else{
          return null;
        }
    }

    //deleteNewBrand
    public static void deleteBrand(Integer brandId){
        List<Brand> list = getAllBrandsFromDAO();
        Brand brand = getBrandById(brandId);
        list.remove(brandId);
        brand.setBrandArrayList(list);
        System.out.println("Size from delete  = "+ list.size());
    }


}
