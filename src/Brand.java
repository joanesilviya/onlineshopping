import java.util.ArrayList;
import java.util.List;

public class Brand {


    private Integer brandId;
    private String brandName;
    private String description;
    private List<Brand> brandArrayList;


    public Brand(Integer id,String name,String desc){
        this.brandId=id;
        this.brandName = name;
        this.description = desc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public List<Brand> getBrandArrayList() {
        return brandArrayList;
    }

    public void setBrandArrayList(List<Brand> brandArrayList) {
        this.brandArrayList = brandArrayList;
    }
}
