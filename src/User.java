public class User {

    private Integer userId;
    private String endUserName;
    private int CustomerType;

    public Integer getUserId() {
        return userId;
    }

    public void setEndUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEndUserName() {
        return endUserName;
    }

    public void setEndUserName(String endUserName) {
        this.endUserName = endUserName;
    }

    public int getCustomerType() {
        return CustomerType;
    }

    public void setCustomerType(int customerType) {
        CustomerType = customerType;
    }





}
