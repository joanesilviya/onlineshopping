import java.util.HashMap;
import java.util.Map;

public class Category {

    private Integer categoryId;
    private String categoryName;
    private String description;

    private Map<Integer,Category> categoryList;

    public Map<Integer, Category> getCategoryList() {
        return categoryList;
    }

    public Category(Integer categoryId,String categoryName,String desc){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.description = desc;
    }

    public void setCategoryList(Map<Integer, Category> categoryList) {
        this.categoryList = categoryList;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
