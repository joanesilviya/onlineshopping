import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserMainApp {

    public static void main(String args[]){

        String cName="Apparels";
        Category category;

        //get Products based on Category
        UserServices.getProductsByCategoryId(2);


        //get all Categories based on Brand
        System.out.println("Displaying Brands for a Category :");
        UserServices.getAllBrandsBasedOnCategory(cName);


       //get all Categories by BrandId
        int brandId =1;
        UserServices.getAllCategoriesByBrandId(brandId);

        //getCategory using CategoryName
        category  = UserServices.findCategoryByName(cName);
        System.out.println("Category Details "+category.getCategoryId() +" Desc "+category.getDescription());


    }
}
