import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserServices {

    public void addToCart(){

    }

    /*
     * This method is used to get all the products by category
     */
    public static void getProductsByCategoryId(Integer categoryId){

        Map<Integer,Product> products = new HashMap<Integer, Product>();
        products = getAllProducts();

        //
        Map<Integer, Product> collect = products.entrySet().stream().filter(x -> x.getValue().getCategoryId() == categoryId)
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
        collect.entrySet().stream().forEach(s->System.out.println(s.getValue().getProductId() +"  " +s.getValue().getName()));

    }



    public static Map<Integer, Product> getAllProducts() {

        Map<Integer,Product> productList = new HashMap<Integer, Product>();
        productList.put( 1, new Product( 1, 1, 1,"Kurti") );
        productList.put( 2,new Product(2,1,1,"Leggins"));
        productList.put( 3, new Product( 3, 1, 2,"Canvas Shoes") );

        productList.put( 4,new Product(4,2,3,"Ear rings"));
        productList.put( 5, new Product( 5, 2, 3,"Kurti") );
        productList.put( 6,new Product(6,2,3,"Watches"));

        productList.put( 7, new Product( 7, 4, 4,"Protien Mix"));
        productList.put( 8,new Product(8,4,4,"Ghee"));

        productList.put( 9,new Product(9,6,5,"Ayur sun screen"));
        productList.put( 10,new Product(10,6,5,"Lotus lotion"));

        productList.put( 11,new Product(11,6,2,"Caps"));
        productList.put( 12,new Product(12,3,2,"Belts"));

        productList.put( 13,new Product(13,2,2,"Belts"));
        productList.put( 14,new Product(14,3,3,"T shirts"));
        productList.put( 15,new Product(15,3,2,"Deodorant"));
        productList.put( 16,new Product(16,5,2,"Salwar Suits"));

        return productList;
    }

    //Hard coding all the values of categories
    // Similar to retrieving all the datas from database
    public static Map<Integer,Category> seedCategoryList(){

        Map<Integer,Category> categoryList = new HashMap<Integer,Category>();
        categoryList.put(1,new Category( 1,"Clothes","all types of clothes" ));
        categoryList.put(2,new Category( 2,"Apparels","all types of apparels" ));
        categoryList.put(3,new Category( 3,"Fashion Accessories","all types of fashion accessories" ));
        categoryList.put(4,new Category( 4,"Food","all types of foods" ));
        categoryList.put(5,new Category( 5,"Health Care","related to beauty and health care" ));

        return categoryList;
    }



    /*
     *  This method is used to get all Categories under each Brand
     */
    public static Map<Integer,List<Integer>> seedBrandsCategories(){
        Map<Integer,List<Integer>> brandCategory = new HashMap<Integer, List<Integer>>();

        brandCategory.put(1,Arrays.asList(1,2));
        brandCategory.put(2,Arrays.asList( 2,3));
        brandCategory.put(3,Arrays.asList(2,3));
        brandCategory.put(4,Arrays.asList(4));
        brandCategory.put(5,Arrays.asList(2));
        brandCategory.put(6,Arrays.asList(5,2));

        return brandCategory;

    }

    //Display Brands which has certainCategory
    public static void getAllBrandsBasedOnCategory(String categoryName){
        Category category;

        Map<Integer, List<Integer>> list = UserServices.seedBrandsCategories();
        Map<Integer,Brand> brandList = new HashMap<Integer, Brand>();

        for (Map.Entry<Integer,List<Integer>> entry : list.entrySet()) {

            for (int i=0; i<entry.getValue().size();i++){
                category = UserServices.findCategoryById(entry.getValue().get(i));

                if(category!=null && category.getCategoryName().equals(categoryName)) {
                    //System.out.println(category.getCategoryName() + " :: "+categoryName);
                    brandList.put( entry.getKey(), BrandServices.getBrandById(entry.getKey()));
                }
            }
        }

        brandList.forEach( (key, value) -> System.out.println(value.getBrandName()));
    }

    //Find Category by name
    public static Category findCategoryByName(String categoryName){

        Map<Integer,Category> categoryMap = seedCategoryList();
        Optional<Map.Entry<Integer, Category>> cat = categoryMap.entrySet().stream()
                .filter( c-> c.getValue().getCategoryName().equals(categoryName)).findAny();


        if(cat.isPresent()){
            return cat.get().getValue();

        }else {
            return null;
        }
    }

    //Find Category by categoryId
    public static Category findCategoryById(int categoryId){
        Optional<Map.Entry<Integer, Category>> category;

        Map<Integer,Category> categoryMap = seedCategoryList();
        category = categoryMap.entrySet().stream().filter(c-> c.getKey().equals(categoryId)).findFirst();
        if(category.isPresent()) {
            return category.get().getValue();
        }else{
            return null;
        }
    }

    // Get all Brands based on Category
    public static void getAllBrandsByCategoryId(Integer categoryId){

        Map<Integer,List<Integer>> brandList = seedBrandsCategories();

    }


    //Get all Categories based on Brand Id
    public static void getAllCategoriesByBrandId(Integer brandId){

        Map<Integer,List<Integer>> brandList = seedBrandsCategories();

        List<Integer> lists= brandList.get(brandId);

        Map<Integer, Category> categoryList = seedCategoryList();
        Optional<Map.Entry<Integer, Category>> category = categoryList.entrySet().stream()
                            .findFirst().filter( map-> map.getKey() == lists.get(0));

        System.out.println("Category Details ="+category.get().getKey()+"  : "+category.get().getValue().getCategoryName());

    }

}
