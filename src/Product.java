import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Product {

    private Integer productId;
    private Integer brandId;
    private Integer categoryId;

    private Integer brandCategoryId;
    private String name;

    private Map<Integer, Product> productList;

    private String productDesc;
    private Integer existingStock;
    private Date manufacturedDate;


    public Product(Integer productId,Integer brandId,Integer categoryId,String name) {
            this.productId=productId;
            this.categoryId = categoryId;
            this.name = name;
    }

    public Map<Integer, Product> getProductList() {
        return productList;
    }

    public void setProductList(Map<Integer, Product> productList) {
        this.productList = productList;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getBrandCategoryId() {
        return brandCategoryId;
    }

    public void setBrandCategoryId(Integer brandCategoryId) {
        this.brandCategoryId = brandCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public Integer getExistingStock() {
        return existingStock;
    }

    public void setExistingStock(Integer existingStock) {
        this.existingStock = existingStock;
    }

    public Date getManufacturedDate() {
        return manufacturedDate;
    }

    public void setManufacturedDate(Date manufacturedDate) {
        this.manufacturedDate = manufacturedDate;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }


}
